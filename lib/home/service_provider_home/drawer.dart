import 'package:flutter/material.dart';
import 'package:geek_doctor/AllWidgets/Divider.dart';
import 'package:geek_doctor/AllWidgets/loading.dart';

import 'package:geek_doctor/home/service_provider_home/profileInfoService.dart';
import 'package:geek_doctor/home/service_provider_home/serviceProviderProfile.dart';
import 'package:geek_doctor/home/service_provider_home/servicemapScreen.dart';
import 'package:geek_doctor/models/serviceprovider_data/serviceprovider.dart';
import 'package:geek_doctor/services/serviceprovider_services/serviceDatabase.dart';
import 'package:provider/provider.dart';

class DrawerCount extends StatefulWidget {
  const DrawerCount({Key? key}) : super(key: key);

  @override
  _DrawerCountState createState() => _DrawerCountState();
}

class _DrawerCountState extends State<DrawerCount> {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<Servicecount?>(context);
    return StreamBuilder<ProviderData>(
        stream: ServiceProviderData(uid: user!.uid).providerData,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            ProviderData? providerData = snapshot.data;
            return Container(
              color: Colors.white,
              width: 300.0,
              child: ListView(
                children: [
                  // Draw Header
                  Container(
                    height: 165.0,
                    child: DrawerHeader(
                      decoration: BoxDecoration(color: Colors.white),
                      child: Row(
                        children: [
                          Image.asset(
                            "images/user_icon.png",
                            height: 65.0,
                            width: 65.0,
                          ),
                          SizedBox(
                            width: 16.0,
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              // ignore: deprecated_member_use
                              FlatButton(
                                onPressed: () {},
                                child: Text(
                                  providerData!.name +
                                      ' ' +
                                      providerData.lastname,
                                  style: TextStyle(
                                      fontSize: 16.0, fontFamily: "Brand-Bold"),
                                ),
                              ),
                              // ignore: deprecated_member_use
                              FlatButton(
                                onPressed: () {
                                  Navigator.pushNamed(
                                      context, ProfileInfoService.idScreen);
                                },
                                child: Text(
                                  'Profile Information',
                                  style: TextStyle(
                                      fontSize: 16.0, fontFamily: "Brand-Bold"),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),

                  DividerWidget(),

                  SizedBox(
                    height: 12.0,
                  ),

                  // Drawer Body Controllers
                  ListTile(
                    leading: Icon(Icons.history),
                    title: Text(
                      "History",
                      style: TextStyle(fontSize: 15.0),
                    ),
                    onTap: () {},
                  ),
                  ListTile(
                    leading: Icon(Icons.person),
                    title: Text(
                      "Edit Profile",
                      style: TextStyle(fontSize: 15.0),
                    ),
                    onTap: () {
                      Navigator.pushNamed(context, ServiceProfile.idScreen);
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.chat, size: 30.0),
                    title: Text(
                      "Messages",
                      style: TextStyle(
                        fontSize: 15.0,
                      ),
                    ),
                    onTap: () {},
                  ),
                  ListTile(
                    leading: Icon(Icons.add_location_sharp),
                    title: Text(
                      "My Current Location",
                      style: TextStyle(
                        fontSize: 12.0,
                      ),
                    ),
                    onTap: () {
                      Navigator.pushNamed(context, ServiceMapScreen.idScreen);
                    },
                  ),

                  ListTile(
                    leading: Icon(Icons.info),
                    title: Text(
                      "About",
                      style: TextStyle(
                        fontSize: 15.0,
                      ),
                    ),
                    onTap: () {},
                  ),
                ],
              ),
            );
          } else {
            return Loading();
          }
        });
  }
}
