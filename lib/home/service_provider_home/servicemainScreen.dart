import 'package:flutter/material.dart';

import 'package:geek_doctor/subScreens/openingScreen.dart';

import '../../services/serviceprovider_services/authServiceProvider.dart';
import 'drawer.dart';

class ServiceMainScreen extends StatelessWidget {
  static const String idScreen = "servicemain";

  final AuthServiceProvider _auth = AuthServiceProvider();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.orange[100],
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'images/geeklogo.png',
              fit: BoxFit.contain,
              height: 42,
            ),
            SizedBox(
              width: 5.0,
            ),
            Container(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'Geek Doctor',
                style: TextStyle(color: Colors.orange),
              ),
            ),
            SizedBox(
              width: 2.0,
            ),
            TextButton.icon(
                icon: Icon(Icons.search, color: Colors.black),
                label: Text(''),
                onPressed: () {}),
            TextButton.icon(
                icon: Icon(Icons.logout, color: Colors.black),
                label: Text(''),
                onPressed: () async {
                  _auth.signOutServiceProvider();
                  Navigator.pushNamedAndRemoveUntil(
                      context, OpeningScreen.idScreen, (route) => false);
                })
          ],
        ),
      ),
      drawer: DrawerCount(),
      body: SingleChildScrollView(
          child: Padding(
        padding: EdgeInsets.symmetric(vertical: 30.0, horizontal: 10.0),
        child: Column(
          children: [
            Text(
              "Nothing to show"
              '\n'
              'Demo Purpose Only'
              '\n'
              'Service Provider Main Screen',
            )
          ],
        ),
      )),
    );
  }
}
