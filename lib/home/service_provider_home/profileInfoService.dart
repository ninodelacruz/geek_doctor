import 'package:flutter/material.dart';
import 'package:geek_doctor/AllWidgets/loading.dart';
import 'package:geek_doctor/AllWidgets/textdecoration.dart';

import 'package:geek_doctor/models/serviceprovider_data/serviceprovider.dart';
import 'package:geek_doctor/services/serviceprovider_services/serviceDatabase.dart';
import 'package:provider/provider.dart';

class ProfileInfoService extends StatelessWidget {
  static const String idScreen = 'profileinfoservice';

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<Servicecount?>(context);
    return StreamBuilder<ProviderData>(
        stream: ServiceProviderData(uid: user!.uid).providerData,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            ProviderData? providerData = snapshot.data;
            return Scaffold(
              backgroundColor: Colors.white,
              appBar: AppBar(
                title: Text('GEEK PROFILE'),
                backgroundColor: Colors.orange[100],
              ),
              body: SingleChildScrollView(
                child: Padding(
                  padding:
                      EdgeInsets.symmetric(vertical: 50.0, horizontal: 8.0),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 10.0,
                      ),
                      Image(
                        image: AssetImage("images/user_icon.png"),
                        width: 390.0,
                        height: 250.0,
                        alignment: Alignment.center,
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      Padding(
                        padding: EdgeInsets.all(5.0),
                        child: Row(
                          children: [],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(5.0),
                        child: Column(
                          children: [
                            new Container(
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  new Flexible(
                                    child: Padding(
                                      padding: const EdgeInsets.all(2.0),
                                      child: new TextFormField(
                                        initialValue: providerData!.name,
                                        decoration: textInputDecoration,
                                      ),
                                    ),
                                  ),
                                  new Flexible(
                                    child: Padding(
                                      padding: const EdgeInsets.all(2.0),
                                      child: new TextFormField(
                                        initialValue: providerData.lastname,
                                        decoration: textInputDecoration,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 1.0,
                            ),
                            SizedBox(
                              height: 1.0,
                            ),
                            TextFormField(
                              initialValue: providerData.address,
                              decoration: textInputDecoration,
                            ),
                            SizedBox(
                              height: 1.0,
                            ),
                            TextFormField(
                              initialValue: providerData.email,
                              decoration: textInputDecoration,
                            ),
                            SizedBox(
                              height: 1.0,
                            ),
                            new TextFormField(
                              initialValue: providerData.contact,
                              decoration: textInputDecoration,
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Align(
                              alignment: Alignment.topLeft,
                              child: Text('Expertise:',
                                  style: TextStyle(
                                    fontSize: 16.0,
                                  )),
                            ),
                            SizedBox(
                              height: 1.0,
                            ),
                            TextFormField(
                              initialValue: '1  ' + providerData.expertise1,
                              decoration: textInputDecoration,
                            ),
                            SizedBox(
                              height: 1.0,
                            ),
                            TextFormField(
                              initialValue: '2  ' + providerData.expertise2,
                              decoration: textInputDecoration,
                            ),
                            SizedBox(
                              height: 1.0,
                            ),
                            TextFormField(
                              initialValue: '3  ' + providerData.expertise3,
                              decoration: textInputDecoration,
                            ),
                            SizedBox(
                              height: 1.0,
                            ),
                            TextFormField(
                              initialValue: '4  ' + providerData.expertise4,
                              decoration: textInputDecoration,
                            ),
                            SizedBox(
                              height: 1.0,
                            ),
                            TextFormField(
                              initialValue: '5  ' + providerData.expertise5,
                              decoration: textInputDecoration,
                            ),
                            SizedBox(
                              height: 30.0,
                            ),
                            // ignore: deprecated_member_use
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          } else {
            return Loading();
          }
        });
  }
}
