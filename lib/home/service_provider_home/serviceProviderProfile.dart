import 'package:flutter/material.dart';
import 'package:geek_doctor/home/service_provider_home/profileInfoTile.dart';

class ServiceProfile extends StatelessWidget {
  static const String idScreen = 'serviceprofile';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text('EDIT PROFILE'),
        backgroundColor: Colors.orange[100],
      ),
      body: ProfileInfoTile(),
    );
  }
}
