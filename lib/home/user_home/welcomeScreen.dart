import 'package:flutter/material.dart';
import 'package:geek_doctor/home/service_provider_home/servicemapScreen.dart';

import 'package:geek_doctor/home/user_home/query_file.dart';

import 'package:geek_doctor/AllWidgets/Divider.dart';
import 'package:geek_doctor/home/user_home/userDrawer.dart';
import 'package:geek_doctor/subScreens/openingScreen.dart';

import '../../services/user_services/auth.dart';

// ignore: must_be_immutable
class WelcomeScreen extends StatelessWidget {
  final AuthService _auth = AuthService();
  static const String idScreen = "welcome";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.orange[100],
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'images/geeklogo.png',
              fit: BoxFit.contain,
              height: 42,
            ),
            SizedBox(
              width: 5.0,
            ),
            Container(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'Geek Doctor',
                style: TextStyle(color: Colors.orange),
              ),
            ),
            SizedBox(
              width: 2.0,
            ),
            TextButton.icon(
                icon: Icon(Icons.search, color: Colors.black),
                label: Text(''),
                onPressed: () {}),
            TextButton.icon(
                icon: Icon(Icons.logout, color: Colors.black),
                label: Text(''),
                onPressed: () async {
                  _auth.signOut();
                  Navigator.pushNamedAndRemoveUntil(
                      context, OpeningScreen.idScreen, (route) => false);
                })
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          // Add your onPressed code here!
          Navigator.pushNamed(context, QueryFile.idScreen);
        },
        label: const Text(
          'BOOK A GEEK',
          style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
        ),
        icon: const Icon(
          Icons.home_repair_service_sharp,
          size: 40.0,
        ),
        backgroundColor: Colors.orange,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      drawer: UserDrawer(),
      body: SingleChildScrollView(
          child: Padding(
        padding: EdgeInsets.symmetric(vertical: 30.0, horizontal: 10.0),
        child: Column(
          children: [
            Text(
              "Nothing to show"
              '\n'
              'Demo Purpose Only'
              '\n'
              'Client Main Screen',
            )
          ],
        ),
      )),
    );
  }
}
