import 'package:flutter/material.dart';
import 'package:geek_doctor/home/user_home/userInfoTile.dart';

class UserProfile extends StatelessWidget {
  static const String idScreen = 'userprofile';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text('EDIT PROFILE'),
        backgroundColor: Colors.orange[100],
      ),
      body: UserInfoTile(),
    );
  }
}
