import 'package:flutter/material.dart';
import 'package:geek_doctor/AllWidgets/loading.dart';
import 'package:geek_doctor/AllWidgets/textdecoration.dart';

import 'package:geek_doctor/models/serviceprovider_data/serviceprovider.dart';
import 'package:geek_doctor/models/user_data/user.dart';
import 'package:geek_doctor/services/serviceprovider_services/serviceDatabase.dart';
import 'package:geek_doctor/services/user_services/database.dart';
import 'package:geek_doctor/upload_files/service_Uploads/servicefile_Upload.dart';
import 'package:provider/provider.dart';

class UserInfoTile extends StatefulWidget {
  @override
  _UserInfoTileState createState() => _UserInfoTileState();
}

class _UserInfoTileState extends State<UserInfoTile> {
  String _name = '';
  String _lastname = '';
  String _address = '';
  String _contact = '';
  String _email = '';

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<Usercount?>(context);
    return StreamBuilder<UserData>(
        stream: DatabaseService(uid: user!.uid).userData,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            UserData? userData = snapshot.data;
            return SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 50.0, horizontal: 8.0),
                child: Column(
                  children: [
                    SizedBox(
                      height: 10.0,
                    ),
                    /* Image(
                      image: AssetImage("images/user_icon.png"),
                      width: 390.0,
                      height: 250.0,
                      alignment: Alignment.center,
                    ),*/
                    GestureDetector(
                      onTap: () {
                        Navigator.pushNamed(
                            context, ServiceFileUpload.idScreen);
                      },
                      child: Container(
                        child: ClipRRect(
                          //borderRadius: BorderRadius.circular(20.0),
                          child: Image.asset('images/user_icon.png',
                              width: 390.0, height: 250.0),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    Padding(
                      padding: EdgeInsets.all(5.0),
                      child: Row(
                        children: [],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(5.0),
                      child: Column(
                        children: [
                          new Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                new Flexible(
                                  child: Padding(
                                    padding: const EdgeInsets.all(2.0),
                                    child: new TextFormField(
                                      initialValue: userData!.name,
                                      decoration: textInputDecoration,
                                      onChanged: (val) =>
                                          setState(() => _name = val),
                                    ),
                                  ),
                                ),
                                new Flexible(
                                  child: Padding(
                                    padding: const EdgeInsets.all(2.0),
                                    child: new TextFormField(
                                      initialValue: userData.lastname,
                                      decoration: textInputDecoration,
                                      onChanged: (val) =>
                                          setState(() => _lastname = val),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 1.0,
                          ),
                          SizedBox(
                            height: 1.0,
                          ),
                          TextFormField(
                            initialValue: userData.address,
                            decoration: textInputDecoration,
                            onChanged: (val) => setState(() => _address = val),
                          ),
                          SizedBox(
                            height: 1.0,
                          ),
                          TextFormField(
                            initialValue: userData.email,
                            decoration: textInputDecoration,
                            onChanged: (val) => setState(() => _email = val),
                          ),
                          SizedBox(
                            height: 1.0,
                          ),
                          new TextFormField(
                            initialValue: userData.contact,
                            decoration: textInputDecoration,
                            onChanged: (val) => setState(() => _contact = val),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Align(
                            alignment: Alignment.topLeft,
                            child: Text('Expertise:',
                                style: TextStyle(
                                  fontSize: 16.0,
                                )),
                          ),

                          SizedBox(
                            height: 30.0,
                          ),
                          // ignore: deprecated_member_use
                          RaisedButton(
                              color: Colors.blueAccent,
                              textColor: Colors.white,
                              child: Container(
                                height: 50.0,
                                child: Center(
                                  child: Text(
                                    "UPDATE",
                                    style: TextStyle(
                                        fontSize: 18.0,
                                        fontFamily: "Brand Bold"),
                                  ),
                                ),
                              ),
                              shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(24.0),
                              ),
                              onPressed: () async {
                                await DatabaseService(uid: user.uid)
                                    .updateUserData(
                                  _name == '' ? userData.name : _name,
                                  _lastname == ''
                                      ? userData.lastname
                                      : _lastname,
                                  _address == '' ? userData.address : _address,
                                  _contact == '' ? userData.contact : _contact,
                                  _email == '' ? userData.email : _email,
                                );
                                Navigator.pop(context);
                              }),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            );
          } else {
            return Loading();
          }
        });
  }
}
