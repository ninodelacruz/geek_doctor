class Usercount {
  final String uid;

  Usercount({required this.uid});
}

class UserData {
  final String uid;
  final String name;
  final String lastname;
  final String address;
  final String contact;
  final String email;

  UserData({
    required this.uid,
    required this.name,
    required this.lastname,
    required this.address,
    required this.contact,
    required this.email,
  });
}
