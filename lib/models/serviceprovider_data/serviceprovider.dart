class Servicecount {
  final String uid;

  Servicecount({required this.uid});
}

class ServiceData {
  final String name;
  final String lastname;
  final String address;
  final String contact;
  final String email;
  final String expertise1;
  final String expertise2;
  final String expertise3;
  final String expertise4;
  final String expertise5;
  ServiceData({
    required this.name,
    required this.lastname,
    required this.address,
    required this.contact,
    required this.email,
    required this.expertise1,
    required this.expertise2,
    required this.expertise3,
    required this.expertise4,
    required this.expertise5,
  });
}

class ProviderData {
  final String uid;
  final String name;
  final String lastname;
  final String address;
  final String contact;
  final String email;
  final String expertise1;
  final String expertise2;
  final String expertise3;
  final String expertise4;
  final String expertise5;

  ProviderData({
    required this.uid,
    required this.name,
    required this.lastname,
    required this.address,
    required this.contact,
    required this.email,
    required this.expertise1,
    required this.expertise2,
    required this.expertise3,
    required this.expertise4,
    required this.expertise5,
  });
}
