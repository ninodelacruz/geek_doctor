import 'package:flutter/material.dart';

import 'package:geek_doctor/authenticate/user_register_login/registerScreen.dart';

import 'package:geek_doctor/authenticate/user_register_login/loginScreen.dart';

// ignore: must_be_immutable
class OpeningScreen extends StatelessWidget {
  static const String idScreen = "opening";
  TextEditingController emailTextEditingController = TextEditingController();
  TextEditingController passwordTextEditingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 120.0, horizontal: 8.0),
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 35.0,
              ),
              Image(
                image: AssetImage("images/geek.png"),
                width: 390.0,
                height: 250.0,
                alignment: Alignment.center,
              ),
              SizedBox(
                height: 1.0,
              ),
              Text(
                "",
                style: TextStyle(fontSize: 24.0, fontFamily: "Brand Bold"),
                textAlign: TextAlign.center,
              ),
              new Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    ElevatedButton(
                      onPressed: () {
                        // Navigate to the second screen using a named route.
                        Navigator.pushNamed(context, RegisterScreen.idScreen);
                      },
                      child: Text('Sign up'),
                    ),
                    ElevatedButton(
                      onPressed: () {
                        // Navigate to the second screen using a named route.
                        Navigator.pushNamed(context, LoginScreen.idScreen);
                      },
                      child: Text('Sign in'),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
