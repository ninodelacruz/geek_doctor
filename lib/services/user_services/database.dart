import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:geek_doctor/models/user_data/user.dart';

class DatabaseService {
  final String uid;
  DatabaseService({required this.uid});
//final  FirebaseFirestore _firestore = FirebaseFirestore.instance;
  //collection reference

  final CollectionReference geekCollection =
      FirebaseFirestore.instance.collection('Client Info');

  get value => null;

  Future updateUserData(String name, String lastname, String address,
      String contact, String email) async {
    return await geekCollection.doc(uid).set({
      'name': name,
      'last name': lastname,
      'address': address,
      'contact number': contact,
      'email add': email,
    });
  }

  UserData _userDataFromSnapshot(DocumentSnapshot snapshot) {
    return UserData(
      uid: uid,
      name: (snapshot.get('name')),
      lastname: (snapshot.get('last name')),
      address: (snapshot.get('address')),
      contact: (snapshot.get('contact number')),
      email: (snapshot.get('email add')),
    );
  }

  Stream<UserData> get userData {
    return geekCollection.doc(uid).snapshots().map(_userDataFromSnapshot);
  }
}
