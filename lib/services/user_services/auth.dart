import 'package:firebase_auth/firebase_auth.dart';
import 'package:geek_doctor/models/user_data/user.dart';

class AuthService {
  final FirebaseAuth _auth = FirebaseAuth.instance;

// create user obj based on firebaseUser

  Usercount? _userFromFirebaseUser(User? user) {
    return user != null ? Usercount(uid: user.uid) : null;
  }

  // auth change user stream
  Stream<Usercount?> get user {
    return _auth.authStateChanges().map(_userFromFirebaseUser);
  }

  // sign in with email & passowrd user
  Future signInWitheEmailAndPassword(String email, String password) async {
    try {
      UserCredential result = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      User? user = result.user;
      return _userFromFirebaseUser(user);
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  // register with email & password user
  Future registerWitheEmailAndPassword(String email, String password) async {
    try {
      UserCredential result = await _auth.createUserWithEmailAndPassword(
          email: email, password: password);
      User? user = result.user;
      //create a new document for the user with the uid

      return _userFromFirebaseUser(user);
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  // sign out
  Future signOut() async {
    try {
      return await _auth.signOut();
    } catch (e) {
      print(e.toString());
      return null;
    }
  }
}
