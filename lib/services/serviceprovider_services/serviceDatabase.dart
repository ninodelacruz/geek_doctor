import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:geek_doctor/models/serviceprovider_data/serviceprovider.dart';

class ServiceProviderData {
  final String uid;
  ServiceProviderData({required this.uid});

  //collection reference

  final CollectionReference geekCollection =
      FirebaseFirestore.instance.collection('Service Info');

  get value => null;

  Future updateServiceData(
    String name,
    String lastname,
    String address,
    String contact,
    String email,
    String expertise1,
    String expertise2,
    String expertise3,
    String expertise4,
    String expertise5,
  ) async {
    return await geekCollection.doc(uid).set({
      'name': name,
      'last name': lastname,
      'address': address,
      'contact number': contact,
      'email add': email,
      'expertise1': expertise1,
      'expertise2': expertise2,
      'expertise3': expertise3,
      'expertise4': expertise4,
      'expertise5': expertise5,
    });
  }

  /* List<ServiceData> _serviceDataSnapshot(QuerySnapshot snapshot) {
    return snapshot.docs.map((doc) {
      return ServiceData(
        name: doc.get('name') ?? '',
        lastname: doc.get('last name') ?? '',
        address: doc.get('address') ?? '',
        contact: doc.get('contact number') ?? '',
        email: doc.get('email add') ?? '',
        expertise1: doc.get('expertise1') ?? '',
        expertise2: doc.get('expertise2') ?? '',
        expertise3: doc.get('expertise3') ?? '',
        expertise4: doc.get('expertise4') ?? '',
        expertise5: doc.get('expertise5') ?? '',
      );
    }).toList();
  }*/

  ProviderData _providerDataFromSnapshot(DocumentSnapshot snapshot) {
    return ProviderData(
      uid: uid,
      name: (snapshot.get('name')),
      lastname: (snapshot.get('last name')),
      address: (snapshot.get('address')),
      contact: (snapshot.get('contact number')),
      email: (snapshot.get('email add')),
      expertise1: (snapshot.get('expertise1')),
      expertise2: (snapshot.get('expertise2')),
      expertise3: (snapshot.get('expertise3')),
      expertise4: (snapshot.get('expertise4')),
      expertise5: (snapshot.get('expertise5')),
    );
  }

  // Stream<List<ServiceData>?> get servicedata {
  //   return geekCollection.snapshots().map(_serviceDataSnapshot);
  //}

  Stream<ProviderData> get providerData {
    return geekCollection.doc(uid).snapshots().map(_providerDataFromSnapshot);
  }
}
