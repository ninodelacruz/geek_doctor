import 'package:flutter/material.dart';

import 'package:geek_doctor/models/serviceprovider_data/serviceprovider.dart';
import 'package:geek_doctor/subScreens/openingScreen.dart';

import 'package:geek_doctor/wrap_verify/verify2.dart';

import 'package:provider/provider.dart';

class Wrapper2 extends StatelessWidget {
  static const String idScreen = 'wrap2';

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<Servicecount?>(context);

// return either Home or Aurhenticate widget
    if (user != null) {
      return Verify2();
    } else {
      return OpeningScreen();
    }
  }
}
