import 'package:flutter/material.dart';

import 'package:geek_doctor/models/user_data/user.dart';
import 'package:geek_doctor/wrap_verify/verify.dart';

import 'package:geek_doctor/wrap_verify/wrap2.dart';
import 'package:provider/provider.dart';

class Wrapper extends StatelessWidget {
  const Wrapper({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<Usercount?>(context);

// return either Home or Aurhenticate widget
    if (user != null) {
      return Verify();
    } else {
      return Wrapper2();
    }
  }
}
