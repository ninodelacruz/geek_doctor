import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';

import 'package:geek_doctor/home/service_provider_home/servicemainScreen.dart';

import 'package:geek_doctor/AllWidgets/progressDialog.dart';
import 'package:geek_doctor/models/serviceprovider_data/serviceprovider.dart';
import 'package:geek_doctor/subScreens/openingScreen.dart';
import 'package:provider/provider.dart';

import '../main.dart';

class Verify2 extends StatefulWidget {
  const Verify2({Key? key}) : super(key: key);

  @override
  _Verify2State createState() => _Verify2State();
}

class _Verify2State extends State<Verify2> {
  @override
  @override
  void initState() {
    _getThingsOnStartup().then((value) {
      _getvalueService.call();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: ProgressDialog(
          message: "Authenticating, Please wait...",
        ));
  }

  Future _getThingsOnStartup() async {
    await Future.delayed(Duration(seconds: 2));
  }

  void _getvalueService() {
    final user = Provider.of<Servicecount?>(context, listen: false);

    serviceRef.child(user!.uid).once().then((DataSnapshot snap) {
      if (snap.value != null) {
        Navigator.pushNamedAndRemoveUntil(
            context, ServiceMainScreen.idScreen, (route) => false);
      } else {
        Navigator.pushNamedAndRemoveUntil(
            context, OpeningScreen.idScreen, (route) => false);
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
  }
}
