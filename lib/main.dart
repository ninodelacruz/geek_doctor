import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';

import 'package:geek_doctor/home/service_provider_home/profileInfoService.dart';
import 'package:geek_doctor/home/service_provider_home/serviceProviderProfile.dart';

import 'package:geek_doctor/home/service_provider_home/servicemainScreen.dart';
import 'package:geek_doctor/subScreens/openingScreen.dart';
import 'package:geek_doctor/subScreens/searchScreen.dart';
import 'package:geek_doctor/upload_files/service_Uploads/servicefile_Upload.dart';
import 'package:geek_doctor/wrap_verify/wrap2.dart';

import 'package:geek_doctor/wrap_verify/wrapper.dart';

import 'package:provider/provider.dart';

import 'home/user_home/UserInfoService.dart';
import 'home/user_home/userProfile.dart';
import 'services/user_services/auth.dart';
import 'models/user_data/user.dart';
import 'services/serviceprovider_services/authServiceProvider.dart';

import 'authenticate/user_register_login/loginScreen.dart';
import 'authenticate/serviceprovider_register_login/loginserviceScreen.dart';
import 'home/service_provider_home/servicemapScreen.dart';

import 'home/user_home/query_file.dart';
import 'authenticate/user_register_login/registerScreen.dart';
import 'authenticate/serviceprovider_register_login/registerserviceScreen.dart';

import 'models/serviceprovider_data/serviceprovider.dart';
import 'wrap_verify/verify.dart';
import 'home/user_home/welcomeScreen.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

DatabaseReference usersRef =
    FirebaseDatabase.instance.reference().child("users");

DatabaseReference serviceRef =
    FirebaseDatabase.instance.reference().child("services");

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        StreamProvider<Usercount?>.value(
            value: AuthService().user, initialData: null),
        StreamProvider<Servicecount?>.value(
            value: AuthServiceProvider().user, initialData: null),
      ],
      child: MaterialApp(
        home: Wrapper(),
        // initialRoute: Wrapper.idScreen,
        routes: {
          UserProfile.idScreen: (context) => UserProfile(),
          UserInfoService.idScreen: (context) => UserInfoService(),
          ServiceFileUpload.idScreen: (context) => ServiceFileUpload(),
          ProfileInfoService.idScreen: (context) => ProfileInfoService(),
          ServiceProfile.idScreen: (context) => ServiceProfile(),
          Wrapper2.idScreen: (context) => Wrapper2(),
          Verify.idScreen: (context) => Verify(),
          OpeningScreen.idScreen: (context) => OpeningScreen(),
          WelcomeScreen.idScreen: (context) => WelcomeScreen(),
          ServiceMapScreen.idScreen: (context) => ServiceMapScreen(),
          SearchScreen.idScreen: (context) => SearchScreen(),
          LoginScreen.idScreen: (context) => LoginScreen(),
          RegisterScreen.idScreen: (context) => RegisterScreen(),
          RegisterServiceScreen.idScreen: (context) => RegisterServiceScreen(),
          LoginServiceScreen.idScreen: (context) => LoginServiceScreen(),
          ServiceMainScreen.idScreen: (context) => ServiceMainScreen(),
          QueryFile.idScreen: (context) => QueryFile(),
        },
        debugShowCheckedModeBanner: false,
      ),
    );
  }
}
