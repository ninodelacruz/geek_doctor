import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:geek_doctor/authenticate/serviceprovider_register_login/registerserviceScreen.dart';

import 'package:geek_doctor/home/user_home/welcomeScreen.dart';
import 'package:geek_doctor/AllWidgets/progressDialog.dart';

import '../../main.dart';
import '../../services/user_services/auth.dart';
import '../../services/user_services/database.dart';
import 'loginScreen.dart';

// ignore: must_be_immutable
class RegisterScreen extends StatelessWidget {
  bool _isHidden = true;
  bool _isHidden1 = true;
  static const String idScreen = "register";

  TextEditingController nameTextEditingController = TextEditingController();
  TextEditingController lastnameTextEditingController = TextEditingController();
  TextEditingController emailTextEditingController = TextEditingController();
  TextEditingController phoneTextEditingController = TextEditingController();
  TextEditingController addressTextEditingController = TextEditingController();
  TextEditingController passwordTextEditingController = TextEditingController();
  TextEditingController passwordconfirmTextEditingController =
      TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.orange[400],
        title: Text(
          "Register as Client",
          style: TextStyle(fontSize: 24.0, fontFamily: "Brand Bold"),
          textAlign: TextAlign.center,
        ),
      ),
      endDrawer: Container(
        color: Colors.white,
        width: 255.0,
        child: Drawer(
          child: ListView(
            children: [
              // Draw Header
              Container(
                height: 165.0,
                child: DrawerHeader(
                  decoration: BoxDecoration(color: Colors.white),
                  child: Row(
                    children: [
                      // ignore: deprecated_member_use
                      FlatButton.icon(
                          onPressed: () {
                            Navigator.pushNamed(
                                context, RegisterServiceScreen.idScreen);
                          },
                          icon: Icon(Icons.home_repair_service_sharp),
                          label: Text(
                            'Register as Service Provider',
                            style:
                                TextStyle(fontSize: 12.0, color: Colors.blue),
                          )),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 50.0, horizontal: 8.0),
          child: Column(
            children: [
              SizedBox(
                height: 20.0,
              ),
              Image(
                image: AssetImage("images/geek.png"),
                width: 390.0,
                height: 250.0,
                alignment: Alignment.center,
              ),
              SizedBox(
                height: 1.0,
              ),
              Padding(
                padding: EdgeInsets.all(5.0),
                child: Row(
                  children: [],
                ),
              ),
              Padding(
                padding: EdgeInsets.all(5.0),
                child: Column(
                  children: [
                    new Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          new Flexible(
                            child: Padding(
                              padding: const EdgeInsets.all(2.0),
                              child: new TextField(
                                controller: nameTextEditingController,
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                  hintText: "First Name (required)",
                                  labelStyle: TextStyle(
                                    fontSize: 14.0,
                                  ),
                                  hintStyle: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 10.0,
                                  ),
                                ),
                                style: TextStyle(fontSize: 14.0),
                              ),
                            ),
                          ),
                          new Flexible(
                            child: Padding(
                              padding: const EdgeInsets.all(2.0),
                              child: new TextField(
                                controller: lastnameTextEditingController,
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                  hintText: "Last Name (required)",
                                  labelStyle: TextStyle(
                                    fontSize: 14.0,
                                  ),
                                  hintStyle: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 10.0,
                                  ),
                                ),
                                style: TextStyle(fontSize: 14.0),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 1.0,
                    ),
                    SizedBox(
                      height: 1.0,
                    ),
                    TextField(
                      controller: addressTextEditingController,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                        hintText: "Enter your Complete Address (required)",
                        labelStyle: TextStyle(
                          fontSize: 14.0,
                        ),
                        hintStyle: TextStyle(
                          color: Colors.grey,
                          fontSize: 10.0,
                        ),
                      ),
                      style: TextStyle(fontSize: 14.0),
                    ),
                    SizedBox(
                      height: 1.0,
                    ),
                    TextField(
                      controller: emailTextEditingController,
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(
                        hintText: "Enter your email (required)",
                        labelStyle: TextStyle(
                          fontSize: 14.0,
                        ),
                        hintStyle: TextStyle(
                          color: Colors.grey,
                          fontSize: 10.0,
                        ),
                      ),
                      style: TextStyle(fontSize: 14.0),
                    ),
                    SizedBox(
                      height: 1.0,
                    ),
                    TextField(
                      controller: phoneTextEditingController,
                      keyboardType: TextInputType.phone,
                      decoration: InputDecoration(
                        hintText: "Enter your phone number (required)",
                        labelStyle: TextStyle(
                          fontSize: 14.0,
                        ),
                        hintStyle: TextStyle(
                          color: Colors.grey,
                          fontSize: 10.0,
                        ),
                      ),
                      style: TextStyle(fontSize: 14.0),
                    ),
                    SizedBox(
                      height: 1.0,
                    ),
                    TextField(
                      controller: passwordTextEditingController,
                      obscureText: _isHidden,
                      decoration: InputDecoration(
                        hintText: "Enter your password (required)",
                        labelStyle: TextStyle(
                          fontSize: 14.0,
                        ),
                        hintStyle: TextStyle(
                          color: Colors.grey,
                          fontSize: 10.0,
                        ),
                        suffix: InkWell(
                          onTap: () {
                            _isHidden = !_isHidden;
                            //
                            // This is the trick
                            //
                            (context as Element).markNeedsBuild();
                          },
                          child: Icon(
                            _isHidden ? Icons.visibility : Icons.visibility_off,
                          ),
                        ),
                      ),
                      style: TextStyle(fontSize: 14.0),
                    ),
                    SizedBox(
                      height: 1.0,
                    ),
                    TextField(
                      controller: passwordconfirmTextEditingController,
                      obscureText: _isHidden1,
                      decoration: InputDecoration(
                        hintText: "Confirm your password (required)",
                        labelStyle: TextStyle(
                          fontSize: 14.0,
                        ),
                        hintStyle: TextStyle(
                          color: Colors.grey,
                          fontSize: 10.0,
                        ),
                        suffix: InkWell(
                          onTap: () {
                            _isHidden1 = !_isHidden1;
                            //
                            // This is the trick
                            //
                            (context as Element).markNeedsBuild();
                          },
                          child: Icon(
                            _isHidden1
                                ? Icons.visibility
                                : Icons.visibility_off,
                          ),
                        ),
                      ),
                      style: TextStyle(fontSize: 14.0),
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    // ignore: deprecated_member_use
                    RaisedButton(
                      color: Colors.blueAccent,
                      textColor: Colors.white,
                      child: Container(
                        height: 50.0,
                        child: Center(
                          child: Text(
                            "SIGN UP",
                            style: TextStyle(
                                fontSize: 18.0, fontFamily: "Brand Bold"),
                          ),
                        ),
                      ),
                      shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(24.0),
                      ),
                      onPressed: () {
                        if (nameTextEditingController.text.length < 1) {
                          displayToastMessage(
                              "First Name is required.", context);
                        } else if (lastnameTextEditingController.text.length <
                            1) {
                          displayToastMessage(
                              "Last Name is required.", context);
                        } else if (addressTextEditingController.text.length <
                            4) {
                          displayToastMessage(
                              "Complete Address is required.", context);
                        } else if (!emailTextEditingController.text
                            .contains("@")) {
                          displayToastMessage(
                              "Email address is not valid.", context);
                        } else if (phoneTextEditingController.text.isEmpty) {
                          displayToastMessage("Phone is required.", context);
                        } else if (passwordTextEditingController.text.length <
                            7) {
                          displayToastMessage(
                              "Password must be atleast 7 characters.",
                              context);
                        } else if (passwordconfirmTextEditingController.text !=
                            passwordTextEditingController.text) {
                          displayToastMessage(
                              "Password does not match.", context);
                        } else {
                          registerNewUser(context);
                        }
                      },
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 1.0,
              ),
              // ignore: deprecated_member_use
              FlatButton(
                onPressed: () {
                  Navigator.pushNamed(context, LoginScreen.idScreen);
                },
                child: Text(
                  "Already have an Account? Sign In Here...",
                ),
              ),
              SizedBox(
                height: 1.0,
              ),
              // ignore: deprecated_member_use
              FlatButton(
                onPressed: () {
                  /*  Navigator.pushNamedAndRemoveUntil(
                      context, ClientloginScreen.idScreen, (route) => false);*/
                },
                child: Text(
                  "By Clicking Sign Up, you agree to GeekDoctor's Terms of Use and acknowledge "
                  '\n'
                  "you have read the Privacy Policy. You also consent to receive calls or SMS messages"
                  '\n'
                  "including by automated dialer from GeekDoctor and its affiliates to the number you"
                  '\n'
                  "provide for informational and/or marketing purposes. Consent to receive marketing"
                  '\n'
                  "messages is not a condition to use GeekDoctor. You understand that you may opt out by"
                  '\n'
                  "texting STOP to 9999.",
                  style: TextStyle(
                    fontSize: 8.0,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  final AuthService _auth = AuthService();

  void registerNewUser(BuildContext context) async {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return ProgressDialog(
            message: "Registering, Please wait...",
          );
        });
    dynamic result = await _auth
        .registerWitheEmailAndPassword(
            emailTextEditingController.text, passwordTextEditingController.text)
        .catchError((errMsg) {
      Navigator.pop(context);

      displayToastMessage("Error: " + errMsg.toString(), context);
    });

    if (result != null) //user HttpStatus.created
    {
      await DatabaseService(uid: result.uid).updateUserData(
        nameTextEditingController.text.trim(),
        lastnameTextEditingController.text.trim(),
        addressTextEditingController.text.trim(),
        phoneTextEditingController.text.trim(),
        emailTextEditingController.text.trim(),
      );

      Map userDataMap = {
        "name": nameTextEditingController.text.trim(),
        "lastname": lastnameTextEditingController.text.trim(),
        "address": addressTextEditingController.text.trim(),
        "email": emailTextEditingController.text.trim(),
        "phone": phoneTextEditingController.text.trim(),
      };

      usersRef.child(result.uid).set(userDataMap);
      displayToastMessage(
          "Congratulations, your account has been created.", context);

      Navigator.pushNamedAndRemoveUntil(
          context, WelcomeScreen.idScreen, (route) => false);
    } else {
      Navigator.pop(context);

      displayToastMessage("Please use another Email! .", context);
    }
  }
}

displayToastMessage(String message, BuildContext context) {
  Fluttertoast.showToast(msg: message);
}
