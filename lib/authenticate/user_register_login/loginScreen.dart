import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geek_doctor/authenticate/user_register_login/registerScreen.dart';

import 'package:geek_doctor/home/user_home/welcomeScreen.dart';
import 'package:geek_doctor/AllWidgets/progressDialog.dart';
import 'package:geek_doctor/subScreens/resetScreen.dart';

import '../../main.dart';
import '../../services/user_services/auth.dart';
import '../serviceprovider_register_login/loginserviceScreen.dart';

// ignore: must_be_immutable
class LoginScreen extends StatelessWidget {
  bool _isHidden = true;
  static const String idScreen = "login";
  TextEditingController emailTextEditingController = TextEditingController();
  TextEditingController passwordTextEditingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.orange[400],
        title: Text(
          "Login as Client",
          style: TextStyle(fontSize: 24.0, fontFamily: "Brand Bold"),
          textAlign: TextAlign.center,
        ),
      ),
      endDrawer: Container(
        color: Colors.white,
        width: 255.0,
        child: Drawer(
          child: ListView(
            children: [
              // Draw Header
              Container(
                height: 165.0,
                child: DrawerHeader(
                  decoration: BoxDecoration(color: Colors.white),
                  child: Row(
                    children: [
                      // ignore: deprecated_member_use
                      FlatButton.icon(
                          onPressed: () {
                            Navigator.pushNamed(
                                context, LoginServiceScreen.idScreen);
                          },
                          icon: Icon(Icons.home_repair_service_sharp),
                          label: Text(
                            'Login as Service Provider',
                            style:
                                TextStyle(fontSize: 12.0, color: Colors.blue),
                          )),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 50.0, horizontal: 8.0),
          child: Column(
            children: [
              SizedBox(
                height: 35.0,
              ),
              Image(
                image: AssetImage("images/geek.png"),
                width: 390.0,
                height: 250.0,
                alignment: Alignment.center,
              ),
              SizedBox(
                height: 1.0,
              ),
              Padding(
                padding: EdgeInsets.all(20.0),
                child: Column(
                  children: [
                    SizedBox(
                      height: 1.0,
                    ),
                    TextField(
                      controller: emailTextEditingController,
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(
                        labelText: "Email",
                        labelStyle: TextStyle(
                          fontSize: 14.0,
                        ),
                        hintStyle: TextStyle(
                          color: Colors.grey,
                          fontSize: 10.0,
                        ),
                      ),
                      style: TextStyle(fontSize: 14.0),
                    ),
                    SizedBox(
                      height: 1.0,
                    ),
                    TextField(
                      controller: passwordTextEditingController,
                      obscureText: _isHidden,
                      decoration: InputDecoration(
                        labelText: "Password",
                        labelStyle: TextStyle(
                          fontSize: 14.0,
                        ),
                        hintStyle: TextStyle(
                          color: Colors.grey,
                          fontSize: 10.0,
                        ),
                        suffix: InkWell(
                          onTap: () {
                            _isHidden = !_isHidden;
                            //
                            // This is the trick
                            //
                            (context as Element).markNeedsBuild();
                          },
                          child: Icon(
                            _isHidden ? Icons.visibility : Icons.visibility_off,
                          ),
                        ),
                      ),
                      style: TextStyle(fontSize: 14.0),
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    // ignore: deprecated_member_use
                    RaisedButton(
                      color: Colors.yellow,
                      textColor: Colors.white,
                      child: Container(
                        height: 50.0,
                        child: Center(
                          child: Text(
                            "Login",
                            style: TextStyle(
                                fontSize: 18.0, fontFamily: "Brand Bold"),
                          ),
                        ),
                      ),
                      shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(24.0),
                      ),
                      onPressed: () {
                        if (!emailTextEditingController.text.contains("@")) {
                          displayToastMessage(
                              "Email address is not valid.", context);
                        } else if (passwordTextEditingController.text.isEmpty) {
                          displayToastMessage(
                              "Password must be atleast 6 characters.",
                              context);
                        } else {
                          loginAndAuthenticateUser(context);
                        }
                      },
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        TextButton(
                          onPressed: () => Navigator.of(context).push(
                              MaterialPageRoute(
                                  builder: (context) => ResetScreen())),
                          child: Text('Forgot Password?'),
                        )
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 1.0,
              ),
              // ignore: deprecated_member_use
              FlatButton(
                onPressed: () {
                  Navigator.pushNamed(context, RegisterScreen.idScreen);
                },
                child: Text(
                  "Do not have an Account? Register Here...",
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  final AuthService _auth = AuthService();

  void loginAndAuthenticateUser(BuildContext context) async {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return ProgressDialog(
            message: "Authenticating, Please wait...",
          );
        });
    dynamic result = await _auth
        .signInWitheEmailAndPassword(
            emailTextEditingController.text, passwordTextEditingController.text)
        .catchError((errMsg) {
      Navigator.pop(context);
      displayToastMessage("Error: " + errMsg.toString(), context);
    });

    if (result != null) //user HttpStatus.created
    {
      usersRef.child(result.uid).once().then((DataSnapshot snap) {
        if (snap.value != null) {
          Navigator.pushNamedAndRemoveUntil(
              context, WelcomeScreen.idScreen, (route) => false);

          displayToastMessage("Welcome to GeekDoctor.", context);
        } else {
          Navigator.pop(context);

          _auth.signOut();

          displayToastMessage(
              "No record exists for this user, Please create new account.",
              context);
        }
      });
    } else {
      Navigator.pop(context);

      displayToastMessage("Error Occured.", context);
    }
  }
}

displayToastMessage(String message, BuildContext context) {
  Fluttertoast.showToast(msg: message);
}
